# AUSL - AUtistic Security Layer

The autistic way to encrypt any kind of communication.

## How does it work

It uses a symmetric encryption to communicate encrypted. The symmetric key is sent encrypted with a asymetric algorithm.

AUSL is actually just a convenient wrapper around GPG.

## Installation

```bash
./autogen.sh
make install
```

## Using AUSL

### Establishing communication

Alice wants to communicate with Bob over a non-secure channel like E-Mail. Suppose Alice has the PGP key 12345678 and Bob has the key 87654321 then the communication would be established as follows:

1. Alice runs `ausl-gen 12345678 87654321`
2. Alice sends the pgp message part of the output to Bob
3. Bob stores the pgp message in the file key.txt
4. Bob runs `ausl-connect 12345678 key.txt`

### Communicating

Suppose Alice and Bob have established communication, then they would communicate in the following way:

1. Alice runs `echo "hello world" | ausl-s 87654321"` and sends the output to Bob
2. Bob either:
  1. Runs `ausl-r 12345678` and enters the message
  2. Stores Alice's message in message.txt and runs `ausl-r 12345678 message.txt`

Now suppose some time goes by and Alice wants to know what she has sent to Bob. Then she would perform the following:

1. Alice retrieves the encrypted message sent to Bob and place it in message.txt
2. Alice runs `ausl-r 87654321 message.txt`. Alternatively Alice might run `ausl-r 87654321` and enters the encrypted message of 1.

## Supported communication methods

Anything you might think of that can transport ASCII letters. Some examples:

* E-Mail
* Paper
* Wood
* Telegram
* Facebook

## Dependencies

* Bash
* GPG
